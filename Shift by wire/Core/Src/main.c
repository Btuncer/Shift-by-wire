/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "can_tools.h"
#include "GolfChecksum.h"
#include "GolfShift.h"
#include "GolfButtonImitation.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan1;
CAN_HandleTypeDef hcan2;

osThreadId defaultTaskHandle;
osThreadId myButtonTaskHandle;
osThreadId myTask03Handle;
osThreadId myTask04Handle;
osThreadId myTask05Handle;
osMessageQId myQueue01Handle;
osMessageQId myQueue02Handle;
osSemaphoreId SartStopSem01Handle;
osSemaphoreId HandBrakeSem02Handle;
osSemaphoreId ParkBrakeSem03Handle;
/* USER CODE BEGIN PV */
uint8_t B5hCanMessage[8], CommandMessage[8];
uint32_t cnt=0;
uint8_t crctest=0;
typedef_B5h B5hStruct;
typedef_Error ErrorHandle;
//uint8_t DesiredShift,AutonomousMode;
typedef_Command Commandstruct;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN1_Init(void);
static void MX_CAN2_Init(void);
void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);
void HandBrake(void const * argument);
void StartStop(void const * argument);
void ParkBrake(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN1_Init();
  MX_CAN2_Init();
  /* USER CODE BEGIN 2 */
	CAN_FilterConfig();
	init_crc_lookup_tables();
	/*Relay is used in NC mode so relay needs to be energised to make open circuit*/
	HAL_GPIO_WritePin(GPIOC, RELAY_COIL_Pin, GPIO_PIN_SET);
  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of SartStopSem01 */
  osSemaphoreDef(SartStopSem01);
  SartStopSem01Handle = osSemaphoreCreate(osSemaphore(SartStopSem01), 1);

  /* definition and creation of HandBrakeSem02 */
  osSemaphoreDef(HandBrakeSem02);
  HandBrakeSem02Handle = osSemaphoreCreate(osSemaphore(HandBrakeSem02), 1);

  /* definition and creation of ParkBrakeSem03 */
  osSemaphoreDef(ParkBrakeSem03);
  ParkBrakeSem03Handle = osSemaphoreCreate(osSemaphore(ParkBrakeSem03), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
	xSemaphoreTake( ParkBrakeSem03Handle, 0 );
	xSemaphoreTake( HandBrakeSem02Handle, 0 );
	xSemaphoreTake( SartStopSem01Handle, 0 );
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of myQueue01 */
  osMessageQDef(myQueue01, 8, typedef_B5h);
  myQueue01Handle = osMessageCreate(osMessageQ(myQueue01), NULL);

  /* definition and creation of myQueue02 */
  osMessageQDef(myQueue02, 8, typedef_Command);
  myQueue02Handle = osMessageCreate(osMessageQ(myQueue02), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myButtonTask */
  osThreadDef(myButtonTask, StartTask02, osPriorityBelowNormal, 0, 128);
  myButtonTaskHandle = osThreadCreate(osThread(myButtonTask), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, HandBrake, osPriorityLow, 0, 128);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* definition and creation of myTask04 */
  osThreadDef(myTask04, StartStop, osPriorityLow, 0, 128);
  myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

  /* definition and creation of myTask05 */
  osThreadDef(myTask05, ParkBrake, osPriorityLow, 0, 128);
  myTask05Handle = osThreadCreate(osThread(myTask05), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.Prediv1Source = RCC_PREDIV1_SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the Systick interrupt time
  */
  __HAL_RCC_PLLI2S_ENABLE();
}

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN1_Init(void)
{

  /* USER CODE BEGIN CAN1_Init 0 */

  /* USER CODE END CAN1_Init 0 */

  /* USER CODE BEGIN CAN1_Init 1 */

  /* USER CODE END CAN1_Init 1 */
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 9;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_6TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_1TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = ENABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = ENABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN1_Init 2 */

  /* USER CODE END CAN1_Init 2 */

}

/**
  * @brief CAN2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN2_Init(void)
{

  /* USER CODE BEGIN CAN2_Init 0 */

  /* USER CODE END CAN2_Init 0 */

  /* USER CODE BEGIN CAN2_Init 1 */

  /* USER CODE END CAN2_Init 1 */
  hcan2.Instance = CAN2;
  hcan2.Init.Prescaler = 9;
  hcan2.Init.Mode = CAN_MODE_NORMAL;
  hcan2.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan2.Init.TimeSeg1 = CAN_BS1_6TQ;
  hcan2.Init.TimeSeg2 = CAN_BS2_1TQ;
  hcan2.Init.TimeTriggeredMode = DISABLE;
  hcan2.Init.AutoBusOff = ENABLE;
  hcan2.Init.AutoWakeUp = DISABLE;
  hcan2.Init.AutoRetransmission = ENABLE;
  hcan2.Init.ReceiveFifoLocked = DISABLE;
  hcan2.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN2_Init 2 */

  /* USER CODE END CAN2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, RELAY_COIL_Pin|LS_SWITCH_2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LS_SWITCH_1_GPIO_Port, LS_SWITCH_1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, HB_PULL_Pin|HB_PUSH_Pin|CAN2_STBY_Pin|CAN1_STBY_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : RELAY_COIL_Pin LS_SWITCH_2_Pin */
  GPIO_InitStruct.Pin = RELAY_COIL_Pin|LS_SWITCH_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : LS_SWITCH_1_Pin */
  GPIO_InitStruct.Pin = LS_SWITCH_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LS_SWITCH_1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : HB_PULL_Pin HB_PUSH_Pin CAN2_STBY_Pin CAN1_STBY_Pin */
  GPIO_InitStruct.Pin = HB_PULL_Pin|HB_PUSH_Pin|CAN2_STBY_Pin|CAN1_STBY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
		
		xQueueReceive( myQueue01Handle, &B5hStruct, portMAX_DELAY );//Task will be blocked until CAN frame is recieved
		
		
		
		if(Commandstruct.AutonomuosMode){
			
			*(uint8_t*)&ErrorHandle = ShiftControl(B5hStruct.CntnSft.ShiftPos, Commandstruct.DesiredShift, &B5hStruct.SwitchMove, &B5hStruct.SwitchRelated);
			if(ErrorHandle.ShiftErr.Changerequied == true){				
				B5hStruct.Checksum = volkswagen_crc(0xB5, *(uint64_t*)&B5hStruct, 8);
				CAN_SendMessage(&hcan2,0xB5,8, (uint8_t *)&B5hStruct);
			}
			else if(ErrorHandle.ShiftErr.intervention == true){
				//if external intervention occurs to shift stick
				CAN_SendMessage(&hcan2,0xB5,8, (uint8_t *)&B5hStruct);
			}
			else{
				
				CAN_SendMessage(&hcan2,0xB5,8, (uint8_t *)&B5hStruct);
			}
			
			
			
		}
		else
		{
			CAN_SendMessage(&hcan2,0xB5,8, (uint8_t *)&B5hStruct);
			
		}
    //osDelay(1);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myButtonTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  {
		xQueueReceive( myQueue02Handle, &Commandstruct, portMAX_DELAY );//Task will be blocked until CAN frame is recieved
		//*(uint64_t*)&Commandstruct = *(uint64_t*)CommandMessage;
		static typedef_Command PreviousCommandState = {0};
		if(B5hStruct.CntnSft.ShiftPos != P && Commandstruct.DesiredShift == P)
		{
			
			if(PreviousCommandState.DesiredShift != P && Commandstruct.DesiredShift == P)
			{
				xSemaphoreGive(ParkBrakeSem03Handle);		
			}
					
		}
		
		if(PreviousCommandState.Handbrake == 0 && Commandstruct.Handbrake != 0)
		{
			xSemaphoreGive(HandBrakeSem02Handle);
		}
		
		if(PreviousCommandState.StartStop == 0 && Commandstruct.StartStop != 0)
		{
			xSemaphoreGive(SartStopSem01Handle);
		}
		
		
		PreviousCommandState = Commandstruct;
		
		
		
   
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_HandBrake */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_HandBrake */
void HandBrake(void const * argument)
{
  /* USER CODE BEGIN HandBrake */
  /* Infinite loop */
  for(;;)
  {
		if(xSemaphoreTake( HandBrakeSem02Handle, portMAX_DELAY ))
		{
			if(Commandstruct.Handbrake == 1)
			{
				vHandBrakePull();
				cnt++;
			}
			else if(Commandstruct.Handbrake == 2)
			{
				vHandBrakePush();
				cnt--;
			}
		}
		
    osDelay(1);
  }
  /* USER CODE END HandBrake */
}

/* USER CODE BEGIN Header_StartStop */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartStop */
void StartStop(void const * argument)
{
  /* USER CODE BEGIN StartStop */
  /* Infinite loop */
  for(;;)
  {
		if(xSemaphoreTake( SartStopSem01Handle, portMAX_DELAY ))
		{
			vStartStop();
			
		}
		
    osDelay(1);
  }
  /* USER CODE END StartStop */
}

/* USER CODE BEGIN Header_ParkBrake */
/**
* @brief Function implementing the myTask05 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ParkBrake */
void ParkBrake(void const * argument)
{
  /* USER CODE BEGIN ParkBrake */
  /* Infinite loop */
  for(;;)
  {
		if(xSemaphoreTake( ParkBrakeSem03Handle, portMAX_DELAY ))
		vParkBrake();
		
    osDelay(1);
  }
  /* USER CODE END ParkBrake */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
