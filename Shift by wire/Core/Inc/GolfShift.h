#ifndef GOLFSHIFT_H
#define GOLFSHIFT_H
#include "stdint.h"

#define P 1
#define R 2
#define N 3
#define D 4
#define S 5

#define NoChange 				1
#define ChangeRequied		2
#define InvalidShift		4
#define	TimeoutError		8
#define Intervention		16

#define HandBrakepull		1
#define HandBrakepush		2

uint8_t ShiftControl(uint8_t CurrentShift, uint8_t DesiredShift, uint8_t *SwitchMove, uint8_t *SwitchRelated);


typedef struct{
	struct{
		uint8_t Nochange 			:1;
		uint8_t Changerequied :1;
		uint8_t Invalidshift 	:1;
		uint8_t Timeouterror 	:1;
		uint8_t intervention 	:1;
		uint8_t rsv					 	:3;
	}ShiftErr;
	
	
}typedef_Error;


typedef struct{

	uint8_t Checksum;
	struct{
		uint8_t Counter :4;
		uint8_t ShiftPos:4;
	}CntnSft;
	uint8_t SwitchMove;
	uint8_t Random;
	uint8_t unknown;
	uint8_t SwitchRelated;
	uint8_t Noidea;
	uint8_t Noidea2;
	
}typedef_B5h;

typedef struct{
	
	uint8_t AutonomuosMode;
	uint8_t StartStop;
	uint8_t DesiredShift;
	uint8_t Handbrake;
	uint8_t Throttle;
	uint8_t rsv1;
	uint8_t rsv2;
	uint8_t rsv3;
}typedef_Command;
	
	




#endif

